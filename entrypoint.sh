#!/bin/bash

# Добавляем адрес хоста в xdebug
sed -i "s/xdebug.remote_host=*.*.*.*/xdebug.remote_host=$(ip route show default | grep -oP '\d+.\d+.\d+.\d+')/" /etc/php/7.3/fpm/php.ini
sed -i "s/xdebug.remote_host=*.*.*.*/xdebug.remote_host=$(ip route show default | grep -oP '\d+.\d+.\d+.\d+')/" /etc/php/7.3/cli/php.ini

# Далее запускаем supervisor
supervisord -n -c /etc/supervisor/supervisord.conf
