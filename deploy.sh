#!/bin/bash

set -e

# Запускаем Деплоер внутри контейнера приложения, он будет все делать
# относительно путей в контейнере.
docker-compose exec php su app -Pc 'dep deploy local'

# Перезапускаем все службы внутри контейнера после успешного деплоя.
docker-compose exec php supervisorctl restart all
